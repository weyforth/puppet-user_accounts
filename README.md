# Puppet User Accounts Module

```ruby
mod "puppetlabs/stdlib", "4.5.1"

# dep: puppetlabs/stdlib
mod "camptocamp/accounts", "1.1.9"

# dep: camptocamp/accounts
mod "weyforth/user_accounts",
	git: "https://bitbucket.org/weyforth/puppet-user_accounts",
	ref: "master"
```
